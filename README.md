﻿Daram Renamer [![works badge](https://cdn.rawgit.com/nikku/works-on-my-machine/v0.2.0/badge.svg)](https://github.com/nikku/works-on-my-machine) [![version badge](https://img.shields.io/badge/version-3.10-green.svg)](https://shields.io)
==============
Filename batch change application.

"다람 리네이머" and "Daram Renamer" is official conventions.

License Details:
http://daram.pe.kr/mit-license

Open source Libraries:
[TagLib#](https://github.com/mono/taglib-sharp), [WPF Task Dialog](https://github.com/yadyn/WPF-Task-Dialog), [WPF Native Folder Browser](https://wpffolderbrowser.codeplex.com)

Icons:
[Metro Studio](http://www.syncfusion.com), [FamFamFam Silk Icon](http://www.famfamfam.com)